import React, { Component } from 'react'
import Item_Shoe from './Item_Shoe'

export default class List_Shoe extends Component {
    render() {
        let list = this.props.list
        return (
            <div className='row'>
                {list.map((item) => {
                    return <Item_Shoe handleAddCard={this.props.handleAddCard} shoe={item} />
                })}
            </div>
        )
    }
}
